#!/usr/bin/env groovy
import com.example.Docker
def call(){
    Docker docker = new Docker(this)
    docker.bumpUpImageVersion()
}
