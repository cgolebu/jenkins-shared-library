#!/usr/bin/env groovy
import com.example.Docker

import javax.print.Doc

def call(String tag){
    Docker docker = new Docker(this)
    return docker.buildDockerImage(tag)
}
