#!/usr/bin/env groovy
package com.example

class Docker implements  Serializable{
    def script
    Docker(script){
        this.script = script
    }

    def buildDockerArtifact(){
        script.sh "mvn clean package"
    }

    def buildDockerImage(String tag){
        script.withCredentials([
                script.usernamePassword(credentialsId: "dockerhub-credentials", usernameVariable: "USER", passwordVariable: "PASS")
        ]){
            script.sh "docker build -t java-maven-app:$tag ."
            script.sh "docker tag java-maven-app:$tag golebu2023/image-registry:java-maven-app-$tag"
            script.sh "echo '${script.PASS}' | docker login -u '${script.USER}' --password-stdin"
            script.sh "docker push golebu2023/image-registry:java-maven-app-$tag"
        }
    }

    def bumpUpImageVersion(){
        script.withCredentials([script.usernamePassword(credentialsId: 'gitlab-credentials',
                usernameVariable: 'USER', passwordVariable: 'PASS' )
        ]){
            script.sh "git config --global user.name 'jenkins'"
            script.sh "git config --global user.email 'jenkins@example.com'"
            script.sh "git remote set-url origin https://${script.USER}:${script.PASS}@https://gitlab.com/cgolebu/devops-pipeline.git"
            script.sh "git commit -m 'ci:version bump'"
            script.sh "git push origin HEAD:main"
        }
    }

}
